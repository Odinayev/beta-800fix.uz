import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/ElementUI';
import './assets/scss/main.scss';
import "./utils/prototypes";
import "./utils/api";

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
