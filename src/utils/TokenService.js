const TOKEN_KEY = 'Authorization';
const REFRESH_TOKEN_KEY = 'Refresh';
const EXPIRE_TIME = 'Expire';



export const token = {
    save(token)
    {
        localStorage.setItem(TOKEN_KEY, token);
    },
    get()
    {
        return localStorage.getItem(TOKEN_KEY);
    },
    remove()
    {
        localStorage.removeItem(TOKEN_KEY);
    },
};



export const refreshToken = {
    save(token)
    {
        localStorage.setItem(REFRESH_TOKEN_KEY, token);
    },
    get()
    {
        return localStorage.getItem(REFRESH_TOKEN_KEY);
    },
    remove()
    {
        localStorage.removeItem(REFRESH_TOKEN_KEY);
    },
};



export const expireTime = {
    save(time)
    {
        localStorage.setItem(EXPIRE_TIME, time);
    },
    get()
    {
        return parseInt(localStorage.getItem(EXPIRE_TIME));
    },
    remove()
    {
        localStorage.removeItem(EXPIRE_TIME);
    },
};



export function removeAllTokens()
{
    token.remove();
    refreshToken.remove();
    expireTime.remove();
}



export default {
    token,
    refreshToken,
    expireTime,
    removeAllTokens,
};