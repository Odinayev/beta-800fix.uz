import store from './../store';

export function tests(text) {
    console.log(text);
}

export function formValidation() {
    let $form = document.querySelector('[data-validation="form"]');

    $form.forEach(el => {
        if (el.type === "text" || el.type === "password") {
            el.classList.add('is-invalid');
            el.classList.remove('is-valid')
        }

        el.addEventListener('input', function (e) {
            if (e.target) {
                el.classList.remove('is-invalid');
                el.classList.add('is-valid')

                el.nextSibling.classList.remove('el-icon-warning-outline')
                el.nextSibling.classList.add('el-icon-circle-check')
            }
        })
    });
}

export function commit(namespace, data) {
    store.commit(namespace, data);
}

export function getters(namespace) {
    return store.getters[namespace];
}