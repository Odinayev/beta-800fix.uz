export const URL_BASE = "https://api.800fix.net/api/front/";
export const URL_IMG = "";

/*____________________ Production ____________________*/
// export const URL_BASE = 'http://172.17.9.237:9080/api/v1/';
// export const URL_IMG = 'http://172.17.9.237:9080';

/*____________________ Test ____________________*/
// export const URL_BASE = 'http://185.211.129.21:9001/v1.0/';
// export const URL_IMG = 'http://185.211.129.21:9001';

export const LANG = "en";

export default {
    URL_BASE,
    URL_IMG,
    LANG,
};