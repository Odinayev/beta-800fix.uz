import Vue from 'vue';
import store from './../store';
import { tests, commit, getters, formValidation } from './library';

/**
 * For Global store
 */
Vue.prototype.$store = store;
/**
 * For test
 */
Vue.prototype.$test_alert = tests;
/**
 * For commit
 */
Vue.prototype.$commit = commit;
/**
 * For getters
 */
Vue.prototype.$getters = getters;

/**
 * For Form Validation
 */
Vue.prototype.$formIsValid = formValidation