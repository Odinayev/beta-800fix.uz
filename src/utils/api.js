import axios from 'axios';
import Vue from 'vue';
import Axios from 'axios';
import router from '../router';
import store from "../store/index";
import { removeAllTokens, token } from '@/utils/TokenService';
import { LANG, URL_BASE } from './defauls';


const instance = Axios.create({
  baseURL: URL_BASE,
  headers: {
    common: {
      'X-Requested-With': 'XMLHttpRequest',
      'device_os': 'WEB',
      'api_version': '1.0',
    }
  }
});

instance.interceptors.request.use(config => {

  config.headers['Accept-Language'] = localStorage.getItem('lang') ? localStorage.getItem('lang') : LANG;
  // config.headers['language'] = localStorage.getItem('lang') ? localStorage.getItem('lang') : LANG;

  if (token.get()) {
    config.headers.token = token.get();
  }


  return config;
}, error => Promise.reject(error));



instance.interceptors.response.use(response => response, error => {

  if (error.response.status === 401) {
    removeAllTokens();
    store.state.permissionList = [];
    router.push({ name: 'auth' });
  }

  // notification.error(error.response.data.error.message);


  return Promise.reject(error);
});



Vue.prototype.$http = instance;
